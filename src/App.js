import { React } from "react";
import "./App.css";
import { BrowserRouter as Router, Link, Route,Switch } from "react-router-dom";
import Todo from "./Components/Todo";
import AllTodos from "./Components/AllTodos";

function App() {
  return (
    <div className="App">
        <h1>Router-example</h1>
        <Router>
          <Switch>
            <Route component={AllTodos} exact path="/"></Route>
            <Route path="/:id" component={Todo}></Route>
          </Switch>
        </Router>
    
    </div>
  );
}

export default App;
