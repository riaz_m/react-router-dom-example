import React from "react";
import { Link, useParams } from "react-router-dom";
import axios from "axios";

const Todo = (props) => {
  const [Todos, setTodos] = React.useState(null);
  React.useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/todos/`)
      .then((data) => setTodos(data.data));
  }, []);


  return (
    <div>
      <p>all todos</p>
      {Todos
        ? Todos.map((item) => (
            <Link to={`${item.id}`}>
              <p>{item.title}</p>
            </Link>
          ))
        : null}
    </div>
  );
};

export default Todo;
