import React from "react";
import axios from "axios";

const Todo = (props) => {
  const id = props.match.params.id;
  const [Todo, setTodo] = React.useState(null);

  React.useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then((data) => setTodo(data.data));
    console.log(Todo);
  }, []);

  return (
    <div>
      <p>some todo{Todo ? Todo.title : null} </p>
    </div>
  );
};

export default Todo;
